module.exports = {
    chainWebpack: config => {
        config.plugin('html').tap(args => {
            args[0].title= '后台管理'
            return args
        })
    },
    lintOnSave: false,
    devServer: {
        proxy: {
            '/api': {
                target: 'http://beishuo.xingrunkeji.cn',
                changeOrigin: true,
                pathRewrite: {
                    '^/api': 'http://beishuo.xingrunkeji.cn'
                }
            }
        }
    },
    publicPath: '/admin/',
    outputDir: 'admin'
}