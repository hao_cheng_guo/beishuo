import axios from 'axios';
import router from '@/router'
import Vue from 'vue';

router.beforeEach((to, from, next) => {

    let islogin = localStorage.getItem('isLogin')
    //console.log('islogin---------',islogin)
    if (islogin) {
        if (to.path == '/login') {
            next({
                path: '/',
            })
        } else {
            next()
        }
    } else {
        next()
        if (to.meta.islogin) {
            next({
                path: '/login',
            })
        } else {
            next()
        }
    }
})
let this_ = new Vue({
    router
});
axios.create({
    // headers: {'Content-Type': 'application/json'},
    timeout: 600000
})
axios.defaults.headers.post['Content-Type'] = 'application/json'

// 请求拦截
axios.interceptors.request.use(config => {
    // 1. 这个位置就请求前最后的配置
    // 2. 当然你也可以在这个位置 加入你的后端需要的用户授权信息
    // console.log('config----------',config)
    return config
}, error => {
    return Promise.reject(error)
})

// 响应拦截
axios.interceptors.response.use(response => {
    // 请求成功
    // 1. 根据自己项目需求定制自己的拦截
    // 2. 然后返回数据
    console.log(response)
    if (response.status==200) {
        if (response.data.status == 1) {
            this_.$message.error(response.data.result.msg);
        }
        if (response.data.result.msg == '未登录') {
            localStorage.removeItem("userinfo");
            // localStorage.removeItem("token");
            localStorage.removeItem("isLogin");
            this_.$router.push('/login')
        }
    }

    return response;
}, error => {
    // 请求失败
    if (error && error.response) {
        switch (error.response.status) {
            case 400:
                // 对400错误您的处理\
                this_.$message.error('400');
                break
            case 401:
                // 对 401 错误进行处理
                this_.$message.error('401');
                break
            case 404:
                // 对 404 错误进行处理
                this_.$message.error('404，页面走丢了~');
                break
            case 500:
                //对 500 错误进行处理
                this_.$message.error('500，服务器走丢了~');
                break
            default:
                // 如果以上都不是的处理
                return Promise.reject(error);
        }
    }

})