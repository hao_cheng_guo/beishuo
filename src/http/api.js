let url = '/web/agent.php?c=site&a=entry&i=2&m=weikez_shopv2&do=web&r='
export default {
    //登录
    login:process.env.VUE_APP_SERVE_URL+url+'login',
    //登出
    loginout:process.env.VUE_APP_SERVE_URL+url+'login.loginout',
    //首页
    home_index:process.env.VUE_APP_SERVE_URL+url+'lineup.home.index',
    
    /*---------------------------------------商户--------------------------------------*/
    //创建商户
    shop_create:process.env.VUE_APP_SERVE_URL+url+'lineup.shop.create',
    //商户列表
    shop_list:process.env.VUE_APP_SERVE_URL+url+'lineup.shop.index',
    //用户列表
    lineup_shop:process.env.VUE_APP_SERVE_URL+url+'lineup.shop.member_list',
    //商铺详情
    lineup_shop_show:process.env.VUE_APP_SERVE_URL+url+'lineup.shop.show',
    //删除
    lineup_shop_destroy:process.env.VUE_APP_SERVE_URL+url+'lineup.shop.destroy',
    //禁用启用
    lineup_shop_update_status:process.env.VUE_APP_SERVE_URL+url+'lineup.shop.update_status',
    //修改商户
    lineup_shop_update:process.env.VUE_APP_SERVE_URL+url+'lineup.shop.update',

    /*---------------------------------------区域代理--------------------------------------*/
    //添加代理
    lineup_proxy_create:process.env.VUE_APP_SERVE_URL+url+'lineup.proxy.create',
    //代理列表
    lineup_proxy_index:process.env.VUE_APP_SERVE_URL+url+'lineup.proxy.index',
    //代理详情
    lineup_proxy_show:process.env.VUE_APP_SERVE_URL+url+'lineup.proxy.show',
    //代理修改
    lineup_proxy_update:process.env.VUE_APP_SERVE_URL+url+'lineup.proxy.update',
    //删除
    lineup_proxy_destroy:process.env.VUE_APP_SERVE_URL+url+'lineup.proxy.destroy',
    //禁用启用
    proxy_update_status:process.env.VUE_APP_SERVE_URL+url+'lineup.proxy.update_status',

    /*---------------------------------------区域代理--------------------------------------*/
    //排单列表
    lineup_gongpai_index:process.env.VUE_APP_SERVE_URL+url+'lineup.gongpai.index',

    //上传文件
    upload:process.env.VUE_APP_SERVE_URL+'/web/index.php?c=utility&a=file&do=upload&upload_type=image&global=&dest_dir=&uniacid=-1&quality=0&group_id=-1'
    
}