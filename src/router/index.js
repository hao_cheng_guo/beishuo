import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'
import Index from '../views/Index/index'

import Login from '../views/Login'

import Merchant from '../views/Merchant/index'
import MerchantList from '../views/Merchant/list'
import MerchantDetail from '../views/Merchant/detail'

import ProxyIndex from '../views/Proxy/index'
import ProxyList from '../views/Proxy/list'
import ProxyDetail from '../views/Proxy/detail'

import PersonnelIndex from '../views/Personnel/index'
import PersonnelList from '../views/Personnel/list'
import PersonnelDetail from '../views/Personnel/detail'

import LineupIndex from '../views/Lineup/index'
import LineupList from '../views/Lineup/list'
import LineupDetail from '../views/Lineup/detail'

import CommissionIndex from '../views/Commission/index'
import CommissionList from '../views/Commission/list'
import CommissionDetail from '../views/Commission/detail'

Vue.use(VueRouter)
const originalPush = VueRouter.prototype.push
VueRouter.prototype.push = function push(location) {
  return originalPush.call(this, location).catch(err => err)
}
const routes = [
	{
		path: '/',
		component: Home,
		children: [
			{
				path: '/',
				name: 'Index',
				component: Index,
				meta: {
					islogin: true
				}
			},
			{
				path:'/merchant/list',
				component:Merchant,
				children:[
					{
						path:'/merchant/list',
						name:'MerchantList',
						component:MerchantList,
						meta:{
							islogin:true,
							p:'/merchant/list'
						}
					},
					{
						path:'/merchant/detail/:id',
						name:'MerchantDetail',
						component:MerchantDetail,
						meta:{
							islogin:true,
							p:'/merchant/list'
						}
					}
				]
			},
			{
				path:'/proxy/list',
				component:ProxyIndex,
				children:[
					{
						path:'/proxy/list',
						name:'ProxyList',
						component:ProxyList,
						meta:{
							islogin:true,
							p:'/proxy/list'
						}
					},
					{
						path:'/proxy/detail/:id',
						name:'ProxyDetail',
						component:ProxyDetail,
						meta:{
							islogin:true,
							p:'/proxy/list'
						}
					}
				]
			},
			{
				path:'/personnel/list',
				component:PersonnelIndex,
				children:[
					{
						path:'/personnel/list',
						name:'PersonnelList',
						component:PersonnelList,
						meta:{
							islogin:true,
							p:'/personnel/list'
						}
					},
					{
						path:'/personnel/detail',
						name:'PersonnelDetail',
						component:PersonnelDetail,
						meta:{
							islogin:true,
							p:'/personnel/list'
						}
					}
				]
			},
			{
				path:'/lineup/list',
				component:LineupIndex,
				children:[
					{
						path:'/lineup/list',
						name:'LineupList',
						component:LineupList,
						meta:{
							islogin:true,
							p:'/lineup/list'
						}
					},
					{
						path:'/lineup/detail',
						name:'LineupDetail',
						component:LineupDetail,
						meta:{
							islogin:true,
							p:'/lineup/list'
						}
					}
				]
			},
			{
				path:'/commission/list',
				component:CommissionIndex,
				children:[
					{
						path:'/commission/list',
						name:'CommissionList',
						component:CommissionList,
						meta:{
							islogin:true,
							p:'/commission/list'
						}
					},
					{
						path:'/commission/detail',
						name:'CommissionDetail',
						component:CommissionDetail,
						meta:{
							islogin:true,
							p:'/commission/list'
						}
					}
				]
			}
		]
	},
	{
		path:'/login',
		name:'Login',
		component:Login
	}
]

const router = new VueRouter({
	// mode: 'history',
	base: process.env.BASE_URL,
	routes
})

export default router
