import Vue from 'vue'

import App from './App.vue'
import router from './router'
import store from './store'
import './assets/css/comm.css'

import VueAMap from 'vue-amap';

Vue.use(VueAMap);

VueAMap.initAMapApiLoader({

  // 高德的keye

  key: 'f7635549292e64814b0e3fcdfe106b4c',

  uiVersion: '1.0.11',// 版本号

  // 插件集合

  plugin: ['AMap.Geocoder', 'AMap.Autocomplete', 'AMap.PlaceSearch', 'AMap.Scale', 'AMap.OverView', 'AMap.Geolocation', 'AMap.MapType', 'AMap.PolyEditor', 'AMap.CircleEditor'],

  // 高德 sdk 版本，默认为 1.4.4

  v: '1.4.4'

});

import SlideVerify from 'vue-monoplasty-slide-verify';

Vue.use(SlideVerify);

import VCharts from 'v-charts'
Vue.use(VCharts)

import ElementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';
Vue.use(ElementUI);

import api from './http/api';
import http from './http/http';
// axios 拦截器
import './http/axios'
// 对后端接口 进行全局注册
Vue.prototype.$api = api;
// 对请求方式 进行全局注册
Vue.prototype.$http = http;

Vue.config.productionTip = false

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
